#!/usr/bin/env python3

import sys

def branch_to_port(branch_name):
    encoded_data = branch_name.encode('utf-8')
    encoded_int = int.from_bytes(encoded_data, 'little')
    return (encoded_int%11587 + 8080)

def main():
    branch_name = sys.argv[1]
    print(branch_to_port(branch_name))

if __name__ == '__main__':
    main()
