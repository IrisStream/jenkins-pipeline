def helper
node{
    checkout scm
    helper = load 'helper.groovy'
}

pipeline{
    agent none
    stages{
        stage("Update Environment"){
            agent {label env.JENKINS_MACHINE}
            steps{
                script{
                    helper.updateEnv()
                    env.PORT = env.DEPLOY_PROD_PORT
                }
            }
        }
        stage("Update tag for repo"){
            agent {label env.BUILD_MACHINE}
            stages{
                stage("Update repository"){
                    steps{
                        sh "./jenkins_scripts/update_repo.sh ${ORIGIN} main"
                    }
                }
                stage("Create new tag"){
                    steps{
                        script{
                            env.TAG = "$gitlabSourceBranch"[8..-1]
                        }
                        sh "./jenkins_scripts/create_new_tag.sh $TAG"
                    }
                }
            }
        }
        stage("Deploy"){
            agent {label env.DEPLOY_MACHINE}
            environment{
                MYSQL_CREDS = credentials('mysql-username-passwd')
            }
            stages{
                stage("Backup database"){
                    steps{
                        sh './jenkins_scripts/backup_db.sh $MYSQL_CREDS_USR $MYSQL_CREDS_PSW prod'
                    }
                }
                stage("Stop current version"){
                    steps{
                        sh '''#!/bin/bash
                            docker stop ${APP_NAME}
                            docker pull ${NEXUS_URL}/${APP_NAME}:${gitlabSourceBranch}_alpha
                            docker tag ${APP_NAME}:${gitlabSourceBranch}_alpha ${APP_NAME}:${gitlabSourceBranch}
                            docker tag ${APP_NAME}:${gitlabSourceBranch}_alpha ${APP_NAME}:latest
                        '''                    
                    }
                }
                stage("Run new version"){
                    steps{
                        sh "./jenkins_scripts/deploy.sh ${APP_NAME} ${gitlabSourceBranch} prod"
                    }
                }
                stage("Check if app run stability"){
                    steps{
                        script{
                            env.DEPLOY_FAIL= """${sh(
                                                returnStatus: true,
                                                script: './jenkins_scripts/health_check.sh ${DEPLOY_URL}:${PORT} ${ATTEMPTS} ${TIMEOUT}'
                                            )}"""
                            if (env.DEPLOY_FAIL == "1"){
                                currentBuild.result = "FAILURE"
                            }
                        }
                    }
                }
                stage("Rollback to previous version"){
                    when{expression{env.DEPLOY_FAIL == "1" || params.TEST_ROLLBACK}}
                    steps{
                        sh "./jenkins_scripts/rollback.sh ${NEXUS_URL} ${APP_NAME} latest"
                    }
                }
                stage("Rollback database"){
                    when{expression{env.DEPLOY_FAIL == "1" || params.TEST_ROLLBACK}}
                    steps{
                        sh './jenkins_scripts/rollback_db.sh $MYSQL_CREDS_USR $MYSQL_CREDS_PSW prod'
                    }
                }
                stage("Update new latest image"){
                    when{expression{env.DEPLOY_FAIL == "0" && !params.TEST_ROLLBACK}}
                    steps{
                        sh "./jenkins_scripts/update_latest_image.sh ${NEXUS_URL} ${APP_NAME} ${gitlabBranch}" 
                        sh "./jenkins_scripts/update_latest_image.sh ${NEXUS_URL} ${APP_NAME} latest"
                    }
                }
            }
        }
    }

    post {
        success {
            script{
                helper.sendTelegram("SUCCESS: ${currentBuild.fullDisplayName}")
                helper.notifySuccess()
                updateGitlabCommitStatus name: 'Final pipeline', state: 'success'
            }
        }
        failure {
            script{
                helper.sendTelegram("FAILURE: ${currentBuild.fullDisplayName}")
                helper.notifyFailure()
                updateGitlabCommitStatus name: 'Final pipeline', state: 'failed'
            }
        }
        aborted {
            script{
                helper.sendTelegram("ABORTED: ${currentBuild.fullDisplayName}")
                helper.notifyAborted()
                updateGitlabCommitStatus name: 'Final pipeline', state: 'canceled'
            }
        }
    }
    options
    {
        timeout(activity: true, time: 3, unit: 'HOURS')
    }
}
