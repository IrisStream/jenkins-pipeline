def updateEnv(){
    //Agents
    env.JENKINS_MACHINE="jenkins-master"
    env.BUILD_MACHINE="build-machine"
    env.DEPLOY_MACHINE="deploy-machine"

    //Folder
    env.SOURCE_DIR="/home/build_user/src/spring-petclinic2"
    env.MR_SOURCE_DIR="/home/build_user/src/spring-petclinic2-mr"
    env.DB_BACKUP_DIR="/home/deploy_user/db_backup/"
    
    //URL
    env.NEXUS_URL="nexus:5000"
    env.MYSQL_URL="mysql-server"
    env.DEPLOY_URL="deploy-host"
    env.DEPLOY_PROD_PORT="8080"
    env.DEPLOY_ALPHA_PORT="8008"

    //Health check
    env.ATTEMPTS=30
    env.TIMEOUT=5


    //App info
    env.APP_NAME='spring-petclinic'
    env.APP_LOG_PATH='/var/log/spring-petclinic'

    // Trigger infor
    env.ORIGIN="git@gitlab.com:IrisStream/spring-petclinic2.git"
}

def notifySuccess(){
    mail to:"ngodaisonn@gmail.com", subject:"SUCCESS: ${currentBuild.fullDisplayName}", body: "Yay, we passed."
}

def notifyFailure(){
    mail to:"ngodaisonn@gmail.com", subject:"FAILURE: ${currentBuild.fullDisplayName}", body: "Boo, we failed."
}

def notifyAborted(){
    mail to:"ngodaisonn@gmail.com", subject:"ABORTED: ${currentBuild.fullDisplayName}", body: "Pipeline have been aborted."
}

def sendTelegram(message) {
    def encodedMessage = URLEncoder.encode(message, "UTF-8")

    withCredentials([string(credentialsId: 'telegramToken', variable: 'TOKEN'),
    string(credentialsId: 'telegramChatId', variable: 'CHAT_ID')]) {

        response = httpRequest (consoleLogResponseBody: true,
                contentType: 'APPLICATION_JSON',
                httpMode: 'GET',
                url: "https://api.telegram.org/bot${TOKEN}/sendMessage?text=$encodedMessage&chat_id=${CHAT_ID}&disable_web_page_preview=true",
                validResponseCodes: '200')
        return response
    }
}

return this
