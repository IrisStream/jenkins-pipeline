def helper
node{
    checkout scm
    helper = load 'helper.groovy'
}

pipeline{
    agent none
    stages{
        stage("Update Environment"){
            agent {label env.JENKINS_MACHINE}
            steps{
                script{
                    helper.updateEnv()
                }
            }
        }
        stage("Build & Test"){
            agent {label env.BUILD_MACHINE}
            stages{
                stage("Update repository"){
                    steps{
                        sh "./jenkins_scripts/update_repo.sh ${ORIGIN} ${gitlabBranch}"
                    }
                }
                stage("Build Source Code"){
                    steps{
                        sh "./jenkins_scripts/build_src.sh"
                    }
                }
                stage("Code Quality Check"){
                    steps{
                        dir(SOURCE_DIR){
                            withSonarQubeEnv(installationName: 'spring-petclinic'){
                                sh "$WORKSPACE/jenkins_scripts/quality_check.sh"
                            }
                        }
                    }
                }
                stage("Quality Gate"){
                    steps{
                        timeout(time: 20, unit: "MINUTES"){ 
                            waitForQualityGate abortPipeline: true
                        }
                    }
                }
                stage("Unit Test"){
                    steps{
                        sh "./jenkins_scripts/unit_test.sh"
                    }
                }
            }
        }
    }
    post {
        success {
            script{
                helper.sendTelegram("SUCCESS: ${currentBuild.fullDisplayName}")
                helper.notifySuccess()
                updateGitlabCommitStatus name: 'Feature pipeline', state: 'success'
            }
        }
        failure {
            script{
                helper.sendTelegram("FAILURE: ${currentBuild.fullDisplayName}")
                helper.notifyFailure()
                updateGitlabCommitStatus name: 'Feature pipeline', state: 'failed'
            }
        }
        aborted {
            script{
                helper.sendTelegram("ABORTED: ${currentBuild.fullDisplayName}")
                helper.notifyAborted()
                updateGitlabCommitStatus name: 'Feature pipeline', state: 'canceled'
            }
        }
    }
    options
    {
        timeout(activity: true, time: 3, unit: 'HOURS')
    }
}
