#!/bin/bash

cat << "EOF"
*** Update Repo ***
 _    _             _         _          _____                     
| |  | |           | |       | |        |  __ \                    
| |  | | _ __    __| |  __ _ | |_  ___  | |__) | ___  _ __    ___  
| |  | || '_ \  / _` | / _` || __|/ _ \ |  _  / / _ \| '_ \  / _ \ 
| |__| || |_) || (_| || (_| || |_|  __/ | | \ \|  __/| |_) || (_) |
 \____/ | .__/  \__,_| \__,_| \__|\___| |_|  \_\\___|| .__/  \___/ 
        | |                                          | |           
        |_|                                          |_|
EOF

source /home/build_user/.bashrc

REMOTE_REPO_URL=$1
BRANCH=$2

pushd $SOURCE_DIR

git fetch --tags --progress ${REMOTE_REPO_URL} +refs/heads/*:refs/remotes/origin/*
git checkout ${BRANCH}
git pull origin ${BRANCH}

popd
