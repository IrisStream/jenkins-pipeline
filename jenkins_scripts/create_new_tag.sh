#!/bin/bash

NEW_TAG=$1

pushd $SOURCE_DIR

git tag -a $NEW_TAG -m "New release for $NEW_TAG"
git push --tags

popd
