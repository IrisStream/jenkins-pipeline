#!/bin/bash

cat << "EOF"
*** Push Artifact ***
 _____              _                       _    _   __              _   
|  __ \            | |         /\          | |  (_) / _|            | |  
| |__) |_   _  ___ | |__      /  \    _ __ | |_  _ | |_  __ _   ___ | |_ 
|  ___/| | | |/ __|| '_ \    / /\ \  | '__|| __|| ||  _|/ _` | / __|| __|
| |    | |_| |\__ \| | | |  / ____ \ | |   | |_ | || | | (_| || (__ | |_ 
|_|     \__,_||___/|_| |_| /_/    \_\|_|    \__||_||_|  \__,_| \___| \__|
EOF

pushd $SOURCE_DIR
mvn clean deploy -DskipTests

source ~/.bashrc
rsync -azP ./target/*.jar deploy_user@deploy-host:$WORKSPACE/jenkins_scripts

popd
