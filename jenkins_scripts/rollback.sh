#!/bin/bash

cat << "EOF"
***Rollback***
 _____         _  _  _                   _    
|  __ \       | || || |                 | |   
| |__) | ___  | || || |__    __ _   ___ | | __
|  _  / / _ \ | || || '_ \  / _` | / __|| |/ /
| | \ \| (_) || || || |_) || (_| || (__ |   < 
|_|  \_\\___/ |_||_||_.__/  \__,_| \___||_|\_\
EOF

REGISTRY_URL=$1
IMAGE_NAME=$2
TAG=$3


echo "Restore old version"
./jenkins_scripts/deploy.sh ${REGISTRY_URL}/${IMAGE_NAME} ${TAG}

echo "Remove broken image"
docker rmi ${IMAGE_NAME}:${TAG}
