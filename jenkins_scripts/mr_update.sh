#!/bin/bash

cat << "EOF"
*** Merge Repo ***
 __  __                           _____                     
|  \/  |                         |  __ \                    
| \  / |  ___  _ __  __ _   ___  | |__) | ___  _ __    ___  
| |\/| | / _ \| '__|/ _` | / _ \ |  _  / / _ \| '_ \  / _ \ 
| |  | ||  __/| |  | (_| ||  __/ | | \ \|  __/| |_) || (_) |
|_|  |_| \___||_|   \__, | \___| |_|  \_\\___|| .__/  \___/ 
                     __/ |                    | |           
                    |___/                     |_|           
EOF

REMOTE_REPO_URL=$1
SOURCE_BRANCH=$2
TARGET_BRANCH=$3

pushd $SOURCE_DIR
git merge --abort
git clean -df
git stash save --keep-index --include-untracked
git fetch --tags --progress ${REMOTE_REPO_URL} +refs/heads/*:refs/remotes/origin/*
git checkout origin/$TARGET_BRANCH
git config user.email 'je@nkins.domain'
git config user.name 'jenkins'

if [ "$(git merge origin/$SOURCE_BRANCH | grep failed)" ];
then
    popd
    exit 1
fi

popd
