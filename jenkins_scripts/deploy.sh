#!/bin/bash

cat << "EOF"
*** Deploy ***
 _____                _               
|  __ \              | |              
| |  | |  ___  _ __  | |  ___   _   _ 
| |  | | / _ \| '_ \ | | / _ \ | | | |
| |__| ||  __/| |_) || || (_) || |_| |
|_____/  \___|| .__/ |_| \___/  \__, |
              | |                __/ |
              |_|               |___/ 
EOF

IMAGE_NAME=$1
TAG=$2
ENV=$3

echo "Set container name"
if [[ "$ENV" == "prod" ]];
then
    CONTAINER_NAME=${APP_NAME}
else
    CONTAINER_NAME=${APP_NAME}_${TAG}
fi

echo "Shutdown & remove old container"

if [ "$(docker ps -aq -f name=$CONTAINER_NAME)" ]; then 
	if (("$(docker container inspect -f '{{.State.Running}}' $CONTAINER_NAME)" == true)); then
        docker stop $CONTAINER_NAME
	fi
	docker rm $CONTAINER_NAME
fi

echo "Run new container"

docker run \
    -e SPRING_PROFILES_ACTIVE=mysql_${ENV} \
    -e MYSQL_USER=${MYSQL_CREDS_USR} \
    -e MYSQL_PASS=${MYSQL_CREDS_PSW} \
        -p ${PORT}:8080 \
            -v ${APP_LOG_PATH}_${TAG}:${APP_LOG_PATH} \
                --name $CONTAINER_NAME \
                    --restart unless-stopped \
                    -d ${IMAGE_NAME}:${TAG}
