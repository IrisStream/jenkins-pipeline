#!/bin/bash

cat << "EOF"
*** Health Check ***
 _    _               _  _    _        _____  _                  _    
| |  | |             | || |  | |      / ____|| |                | |   
| |__| |  ___   __ _ | || |_ | |__   | |     | |__    ___   ___ | | __
|  __  | / _ \ / _` || || __|| '_ \  | |     | '_ \  / _ \ / __|| |/ /
| |  | ||  __/| (_| || || |_ | | | | | |____ | | | ||  __/| (__ |   < 
|_|  |_| \___| \__,_||_| \__||_| |_|  \_____||_| |_| \___| \___||_|\_\
EOF

url=$1
attempts=$2
timeout=$3
online=false

echo "Checking status of $url."

for (( i=1; i<=$attempts; i++ ))
do
  code=`curl -sL --connect-timeout 20 --max-time 30 -w "%{http_code}\\n" "$url" -o /dev/null`

  echo "Found code $code for $url."

  if [ "$code" = "200" ]; then
    echo "Website $url is online."
    online=true
    break
  else
    echo "Website $url seems to be offline. Waiting $timeout seconds."
    sleep $timeout
  fi
done

if $online; then
  echo "Monitor finished, website is online."
  exit 0
else
  echo "Monitor failed, website seems to be down."
  exit 1
fi
