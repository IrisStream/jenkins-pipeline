#!/bin/bash

cat << "EOF"
*** Rollback DB ***
 _____         _  _  _                   _      _____   ____  
|  __ \       | || || |                 | |    |  __ \ |  _ \ 
| |__) | ___  | || || |__    __ _   ___ | | __ | |  | || |_) |
|  _  / / _ \ | || || '_ \  / _` | / __|| |/ / | |  | ||  _ < 
| | \ \| (_) || || || |_) || (_| || (__ |   <  | |__| || |_) |
|_|  \_\\___/ |_||_||_.__/  \__,_| \___||_|\_\ |_____/ |____/ 
EOF

UNAME=$1
PASSWD=$2 
ENV=$3

# Restore databases
pushd $DB_BACKUP_DIR

mysql -hmysql-server -u${UNAME} -p${PASSWD} petclinic_$ENV < reset_$ENV.sql

mysql -hmysql-server -u${UNAME} -p${PASSWD} petclinic_$ENV < backup_$ENV.sql

popd
