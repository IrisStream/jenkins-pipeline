#!/bin/bash

cat << "EOF"
*** Backup DB ***
 ____                _                    _____   ____  
|  _ \              | |                  |  __ \ |  _ \ 
| |_) |  __ _   ___ | | __ _   _  _ __   | |  | || |_) |
|  _ <  / _` | / __|| |/ /| | | || '_ \  | |  | ||  _ < 
| |_) || (_| || (__ |   < | |_| || |_) | | |__| || |_) |
|____/  \__,_| \___||_|\_\ \__,_|| .__/  |_____/ |____/ 
                                 | |                    
                                 |_|                    
EOF

UNAME=$1
PASSWD=$2 
ENV=$3

pushd $DB_BACKUP_DIR

mysqldump --single-transaction --no-tablespaces -hmysql-server -u${UNAME} -p${PASSWD} petclinic_$ENV > backup_$ENV.sql

popd
