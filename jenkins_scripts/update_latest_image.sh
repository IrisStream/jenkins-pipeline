#!/bin/bash

cat << "EOF"
***Update Latest Image***
 _    _             _         _          _             _              _   
| |  | |           | |       | |        | |           | |            | |  
| |  | | _ __    __| |  __ _ | |_  ___  | |      __ _ | |_  ___  ___ | |_ 
| |  | || '_ \  / _` | / _` || __|/ _ \ | |     / _` || __|/ _ \/ __|| __|
| |__| || |_) || (_| || (_| || |_|  __/ | |____| (_| || |_|  __/\__ \| |_ 
 \____/ | .__/  \__,_| \__,_| \__|\___| |______|\__,_| \__|\___||___/ \__|
        | |                                                               
        |_|
EOF

REGISTRY_URL=$1
IMAGE_NAME=$2
TAG=$3

echo "Tagging image with docker registry url"
docker tag ${IMAGE_NAME}:${TAG} ${REGISTRY_URL}/${IMAGE_NAME}:${TAG}

echo "push image to registry"
docker push ${REGISTRY_URL}/${IMAGE_NAME}:${TAG}
