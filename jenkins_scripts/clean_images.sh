#!/bin/bash

echo "Clean up unused images for 12h"

echo "softlink log file"
rm $APP_LOG_PATH
sudo ln -s ${APP_LOG_PATH}_${TAG} ${APP_LOG_PATH}

docker image prune \
    -a \
    --force \
    --filter "until=12h"
